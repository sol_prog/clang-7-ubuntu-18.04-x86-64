# Clang 7.0.0 binary for Ubuntu 18.04 x86-64#

This is my build version of Clang 7.0.0 for Ubuntu 18.04 64 bits. If you prefer to compile it yourself check [https://solarianprogrammer.com/building-clang-libcpp-ubuntu-linux/](https://solarianprogrammer.com/building-clang-libcpp-ubuntu-linux/)


**Clone (download) the archive on your machine with:**

git clone https://sol_prog@bitbucket.org/sol_prog/clang-7-ubuntu-18.04-x86-64.git

I highly recommend that you read my article, you will need to install some prerequisites and modify your system PATH. You can skip the actual build part.

In order to install this binary extract clang_7.0.0.tar.xz and move the extracted *clang_7.0.0* to your */usr/local* folder. After this, install some prerequisites and modify your system path:

```console
sudo apt install build-essential subversion cmake python3-dev libncurses5-dev libxml2-dev libedit-dev swig doxygen graphviz xz-utils

echo 'export PATH=/usr/local/clang_7.0.0/bin:$PATH' >> ~/.bashrc
echo 'export LD_LIBRARY_PATH=/usr/local/clang_7.0.0/lib:LD_LIBRARY_PATH' >> ~/.bashrc
source ~/.bashrc
```

